#include "Test.h"
#include <ctime>

USING_NS_CC;

// Размер матрицы M
#define MAX_X	50
#define MAX_Y	30
// Максимально допустимый перепад высоты
#define MAX_H	71

Scene* Test::createScene()
{
	auto scene = Scene::create();
	scene->addChild(Test::create());
	return scene;
}

// on "init" you need to initialize your instance
bool Test::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	//////////////////////////////
	// 2. Инициализируем матрицу unsigned char М[MAX_X][MAX_Y] случайными числами
	// и создаем спрайты, соответвующие клеткам матрицы

	std::srand(std::time(NULL));
	this->_M.reserve(MAX_X * MAX_Y);
	this->_status.resize(MAX_X * MAX_Y);

	Size winSize = Director::getInstance()->getWinSize();
	Rect spriteRect(0, 0, winSize.width / MAX_X, winSize.height / MAX_Y);

	for (int y = 0; y < MAX_Y; ++y)
	for (int x = 0; x < MAX_X; ++x)
	{
		uint8_t color = std::rand() % 256;
		this->_M.push_back(color);

		auto sprite = Sprite::create();
		sprite->setTextureRect(spriteRect);
		sprite->setColor(Color3B(color, color, color));
		sprite->setAnchorPoint(Vec2::ZERO);
		sprite->setPosition(Vec2(spriteRect.size.width * x, spriteRect.size.height * y));
		this->addChild(sprite);
	}
	
	//////////////////////////////
	// 3. Добавим Touch события для обработки нажатия на спрайт
	auto listenerTouch = EventListenerTouchOneByOne::create();
	listenerTouch->onTouchBegan = [](Touch* touch, Event* event){
		return true;
	};
	listenerTouch->onTouchEnded = [=](Touch* touch, Event* event){
		// Вычисляем индекс нажатой клетки
		uint8_t x = touch->getLocation().x / spriteRect.size.width;
		uint8_t y = touch->getLocation().y / spriteRect.size.height;
		int index = y * MAX_X + x;
		// Находим все достижимые клетки из данной точки
		repaintSprites(index);
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerTouch, this);
	return true;
}

// Меняем цвет спрайтов
void Test::repaintSprites(int startIndex)
{
	auto sprites = this->getChildren();
	if (startIndex < 0 || startIndex >= sprites.size()) return;
	
	// Обнуляем статус клеток
	std::fill(_status.begin(), _status.end(), STATUS::UNAVAILABLE);
	_status[startIndex] = STATUS::AVAILABLE;
	
	// Делаем волновой обход матрицы в поисках доступных клеток по высоте
	std::vector<int> wave, tempWave;
	wave.push_back(startIndex);
	while (wave.size())
	{
		tempWave = wave;
		wave.clear();
		// Проходим всю волну в поисках новых клеток
		for (const auto pos : tempWave)
		{
			// Цикл по четырем сторонам клетки
			for (int step = 0; step < 4; ++step)
			{
				int newPos = -1;
				switch (step)
				{
				case 0:	// Проверяем шаг ВВЕРХ
					if (pos + MAX_X < MAX_X * MAX_Y)
						newPos = pos + MAX_X;
					break;
				case 1:	// Проверяем шаг ВЛЕВО
					if (pos % MAX_X != 0)
						newPos = pos - 1;
					break;
				case 2:	// Проверяем шаг ВПРАВО
					if (pos % MAX_X != MAX_X - 1)
						newPos = pos + 1;
					break;
				case 3:	// Проверяем шаг ВНИЗ
					if (pos - MAX_X >= 0)
						newPos = pos - MAX_X;
					break;
				}
				// Проверяем доступность новой клетки по высоте
				if (newPos >= 0 && _status[newPos] == STATUS::UNAVAILABLE)
				{
					if (abs(_M[pos] - _M[newPos]) < MAX_H)
					{
						_status[newPos] = STATUS::AVAILABLE;
						wave.push_back(newPos);
					}
				}
			}
		}
	}

	// Меняем цвет спрайтов
	for (int i = 0; i < MAX_X * MAX_Y; ++i)
	{
		uint8_t color = this->_M[i];
		sprites.at(i)->setColor(_status[i] == STATUS::AVAILABLE ? Color3B(0, color, 0) : Color3B(color, color, color));
	}
}
