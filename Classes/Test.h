#ifndef __TEST_SCENE_H__
#define __TEST_SCENE_H__

#include "cocos2d.h"

class Test : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	CREATE_FUNC(Test);

	void repaintSprites(int startIndex);

private:
	enum class STATUS { UNAVAILABLE, AVAILABLE };
	std::vector<STATUS> _status;
	std::vector<uint8_t> _M;
};

#endif // __TEST_SCENE_H__
